#NBC Ncredible Client Consulting Workshop

* Was interesting to see how game ideas come out of large companies
    - someone at the company as an idea that involves a game and they think it would be a good thing to make, they get a budget to work on a prototype.
* Surprised a the budget provided to the team to make the prototype
* The influence of data driven design on the production of the prototype is disheartening. There's a lot of time spent on developing features in pursuit of viral capabilities before there is any means to test whether they are effective of if the core game design is compelling enough to warrant sharing in the first place
* Making an app and then trying to make it fun after the fact is difficult. At the same time it brings up the question of whether the users of an app like this actually care to play a "fun game"
* One of the great thing about indie game developers is that they can have a clear game design vision that allows for unique experiences. NCredible feels diluted by too many stakeholders with purely monetary concerns that starve the game of an ability to explore exciting design spaces before business concerns.
* I'm sympathetic to the developers behind the game. They're still passionate about the project and it's easy to see myself in their shoes when they are talking about the project.
* There's a fine line between running a workshop for stdents via a real company and getting free labor out of students. At the same time there's a certain freedom afforded when you know that you have nothing to lose by coming up with some hairbrained idea. 
* Its much like waht making games in sie a game design program is like.
* thinking about the problem of content creation for Ncredible was one of the most interesting parts of the game.
    - News items are ephemeral
    - Interesting questions are hard to write
    - Someone has to be informed about the topic in order to create interesting questions
    - Do you limit the users' velocity through your content bases on how quickly you can make the content, or do you try to come up with a method of creating content more quickly to adapt to how your users consume the content you create?
* Talking to developers of a game that are outside of the "game design community" is interesting.
* It's hard not to fall into a teacher/student mindset while at a workshop at a university alongside students
* I'm surprised y how little substantive playtesting happens for the game outside of family and friends
* Consulting is fun and refreshing. It's nice to think about a design problem for a project that you do not need to then develop yourself. It provides you with freedom to think of ideas and solutions without ultimate responsibility.